package controllers

import models.UserData

import javax.inject._
import play.api._
import play.api.data.Form
import play.api.data.Forms._
import play.api.data.validation.Constraints._
import play.api.mvc._
import play.api.i18n.I18nSupport

@Singleton
class UsersController @Inject()(val controllerComponents: ControllerComponents) extends BaseController with I18nSupport {

  val userForm: Form[UserData] = Form(
    mapping(
      "Name" -> nonEmptyText,
      "Age" -> number(min = 1, max = 100),
      "Email" -> email,
      "Phone" -> text.verifying(pattern("""[0-9.+]+""".r, error = "Please enter valid phone number.")),
      "ID" -> ignored(1)
    )(UserData.apply)(UserData.unapply)
  )

  val deleteForm: Form[UserData] = Form(
    mapping(
      "Name" -> ignored(""),
      "Age" -> ignored(1),
      "Email" -> ignored(""),
      "Phone" -> ignored(""),
      "ID" -> ignored(1)
    )(UserData.apply)(UserData.unapply)
  )

  val editForm: Form[UserData] = userForm.fill(UserData("Test", 18, "abc@gmail.com", "123", 1))

  def users(): Action[AnyContent] = Action { implicit request =>
    Ok(views.html.users())
  }

  def addUser(): Action[AnyContent] = Action { implicit request =>
    editForm.bindFromRequest().fold(
      formWithErrors => {
        BadRequest(views.html.addUser(formWithErrors))
      },
      user => {
        UserData.add(user)
        Redirect(routes.UsersController.users())
      }
    )
  }

  def editUser(): Action[AnyContent] = Action { implicit request =>
    editForm.bindFromRequest().fold(
      formWithErrors => {
        BadRequest(views.html.editUser(formWithErrors))
      },
      user => {
        UserData.edit(user)
        Redirect(routes.UsersController.users())
      }
    )
  }

  def deleteUser(): Action[AnyContent] = Action { implicit request =>
    deleteForm.bindFromRequest().fold(
      formWithErrors => {
        BadRequest(views.html.deleteUserTemplate(formWithErrors))
      },
      user => {
        UserData.delete(user)
        Redirect(routes.UsersController.users())
      }
    )
  }
}
