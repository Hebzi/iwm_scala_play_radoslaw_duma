package models

import scala.collection.mutable.ListBuffer

case class UserData(name: String, age: Int, email: String, phone: String, ID: Int)

case object UserData {

  var users: ListBuffer[UserData] = ListBuffer()
  var id = 1

  def add(user: UserData): Any = {
    if (user.phone.startsWith("+48")) {
      users += user.copy(ID = id)
      id += 1
    } else {
      users += user.copy(ID = id, phone = "+48" + user.phone)
      id += 1
    }
  }

  def setIDs(): Unit = {
    for (i <- 1 to users.length) {
      users(i - 1) = users(i - 1).copy(ID = i)
    }
  }

  def delete(user: UserData): Unit = {
    users.remove(user.ID - 1)
    id -= 1
    setIDs()
    for (i <- users) {
      println(i.ID)
    }
  }

  def edit(user: UserData): Unit = {
    users.remove(user.ID - 1)
    users += user
    setIDs()
  }
}

